package com.shajzlabs.android.app.grl;

import android.app.Application;

import timber.log.Timber;

public class GRLApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
    }
}
