package com.shajzlabs.android.app.grl.activities;

import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.shajzlabs.android.app.grl.R;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import timber.log.Timber;

public class GestureActivity extends AppCompatActivity implements GestureOverlayView.OnGesturePerformedListener {

    private GestureLibrary gestureLibrary;
    @InjectView(R.id.gestureOverlayView) GestureOverlayView gestureOverlayView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gesture);
        ButterKnife.inject(this);

        gestureLibrary = GestureLibraries.fromRawResource(this, R.raw.gestures);
        if (!gestureLibrary.load()) {
            Timber.d("Gestures not loaded");
        }

        gestureOverlayView.addOnGesturePerformedListener(this);
    }

    @Override
    public void onGesturePerformed(GestureOverlayView gestureOverlayView, Gesture gesture) {
        ArrayList<Prediction> predictions = gestureLibrary.recognize(gesture);

        if (predictions.size() > 0 && predictions.get(0).score > 1.0) {
            String result = predictions.get(0).name;

            if ("Circle".equalsIgnoreCase(result)) {
                Toast.makeText(this, "Circle", Toast.LENGTH_LONG).show();
            }
            else if ("Ellipse".equalsIgnoreCase(result)) {
                Toast.makeText(this, "Ellipse", Toast.LENGTH_LONG).show();
            }
            else if ("Rectangle".equalsIgnoreCase(result)) {
                Toast.makeText(this, "Rectangle", Toast.LENGTH_LONG).show();
            }
            else if ("Task".equalsIgnoreCase(result)) {
                Toast.makeText(this, "Task", Toast.LENGTH_LONG).show();
            }
            else if ("Rounded Rectangle".equalsIgnoreCase(result)) {
                Toast.makeText(this, "Rounded Rectangle", Toast.LENGTH_LONG).show();
            }
        }
    }
}
