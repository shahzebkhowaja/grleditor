package com.shajzlabs.android.app.grl.activities;

import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.mindfusion.diagramming.Diagram;
import com.mindfusion.diagramming.DiagramView;
import com.mindfusion.diagramming.NodeListView;
import com.mindfusion.diagramming.SolidBrush;
import com.mindfusion.drawing.Color;
import com.mindfusion.drawing.Dimension;
import com.shajzlabs.android.app.grl.R;
import com.shajzlabs.android.app.grl.nodes.BeliefNode;
import com.shajzlabs.android.app.grl.nodes.GoalNode;
import com.shajzlabs.android.app.grl.nodes.NodeBehavior;
import com.shajzlabs.android.app.grl.nodes.ResourceNode;
import com.shajzlabs.android.app.grl.nodes.SoftGoalNode;
import com.shajzlabs.android.app.grl.nodes.TaskNode;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import timber.log.Timber;


public class MainActivity extends AppCompatActivity implements GestureOverlayView.OnGesturePerformedListener {

    public static final int DEFAULT_NODE_WIDTH = 45;
    public static final int DEFAULT_NODE_HEIGHT = 23;

    private GestureLibrary gestureLibrary;
    private Diagram diagram;
    @InjectView(R.id.diagram_view)
    DiagramView diagramView;
    @InjectView(R.id.node_list_view)
    NodeListView nodeListView;
    @InjectView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @InjectView(R.id.gestureOverlayView)
    GestureOverlayView gestureOverlayView;
    private MenuItem gestureMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        gestureLibrary = GestureLibraries.fromRawResource(this, R.raw.gestures);
        if (!gestureLibrary.load()) {
            Timber.d("Gestures not loaded");
        }

        gestureOverlayView.addOnGesturePerformedListener(this);

        initUI();
    }

    private void initUI() {

        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        registerNodes();
        diagram = diagramView.getDiagram();
        diagram.getUndoManager().setUndoEnabled(true);
        diagram.setBackBrush(new SolidBrush(Color.WHITE));
        // enable drawing OrgChartNodes using touch input
        diagramView.setCustomBehavior(new NodeBehavior(diagramView));

        nodeListView.setDividerColor(R.color.divider);
        nodeListView.setDefaultNodeSize(new Dimension(DEFAULT_NODE_WIDTH, DEFAULT_NODE_HEIGHT));
        nodeListView.setIconSize(new Dimension(100, 50));
        nodeListView.setIconMargin(new Dimension(30, 30));
        nodeListView.setDiagramView(diagramView);

        diagramView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return false;
            }
        });


        addHeaderToList("Intentional Elements");
        addNodesToList();
    }

    private void registerNodes() {
        Diagram.registerClass(GoalNode.class, GoalNode.class.getSimpleName(), 1);
        Diagram.registerClass(SoftGoalNode.class, SoftGoalNode.class.getSimpleName(), 1);
        Diagram.registerClass(SoftGoalNode.class, TaskNode.class.getSimpleName(), 1);
        Diagram.registerClass(ResourceNode.class, ResourceNode.class.getSimpleName(), 1);
        Diagram.registerClass(ResourceNode.class, BeliefNode.class.getSimpleName(), 1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_undo:
                diagram.getUndoManager().undo();
                return true;
            case R.id.action_redo:
                diagram.getUndoManager().redo();
                return true;
            case R.id.action_clear:
                diagram.getItems().remove(diagram.getActiveItem());
                return true;
            case R.id.action_palette:
                toggleFilterDrawerVisibility();
                return true;
            case R.id.action_gesture:
                gestureMenuItem = item;
                toggleGestureView(item);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void toggleFilterDrawerVisibility() {
        if(drawerLayout.isDrawerOpen(Gravity.END)) {
            drawerLayout.closeDrawer(Gravity.END);
        } else {
            drawerLayout.openDrawer(Gravity.END);
        }
    }

    private void addNodesToList() {
        // add nodes to list view
        GoalNode goalNode = new GoalNode(diagram, this);
        nodeListView.addNode(goalNode, GoalNode.DEFAULT_TEXT);

        SoftGoalNode softGoalNode = new SoftGoalNode(diagram, this);
        nodeListView.addNode(softGoalNode, SoftGoalNode.DEFAULT_TEXT);

        TaskNode taskNode = new TaskNode(diagram, this);
        nodeListView.addNode(taskNode, TaskNode.DEFAULT_TEXT);

        ResourceNode resourceNode = new ResourceNode(diagram, this);
        nodeListView.addNode(resourceNode, ResourceNode.DEFAULT_TEXT);

        BeliefNode beliefNode = new BeliefNode(diagram, this);
        nodeListView.addNode(beliefNode, BeliefNode.DEFAULT_TEXT);
    }

    private void addHeaderToList(String text) {
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_list_header, null);
        TextView textView = (TextView) view.findViewById(R.id.listViewHeaderTitle);
        textView.setText(text);

        nodeListView.addHeaderView(view);
    }

    private void toggleGestureView(MenuItem item) {
        if (gestureOverlayView.getVisibility() != View.VISIBLE) {
            item.setIcon(R.drawable.ic_gesture_white_selected);
            gestureOverlayView.setVisibility(View.VISIBLE);
        }
        else {
            item.setIcon(R.drawable.ic_gesture_white);
            gestureOverlayView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onGesturePerformed(GestureOverlayView gestureOverlayView, Gesture gesture) {
        ArrayList<Prediction> predictions = gestureLibrary.recognize(gesture);

        if (predictions.size() > 0 && predictions.get(0).score > 1.0) {
            String result = predictions.get(0).name;

            float centerX = 50;
            float centerY = 50;

            if ("SoftGoal".equalsIgnoreCase(result)) {
                Toast.makeText(this, "SoftGoal", Toast.LENGTH_LONG).show();
                SoftGoalNode softGoalNode = new SoftGoalNode(diagram, this);
                softGoalNode.setBounds(centerX, centerY,
                        DEFAULT_NODE_WIDTH, DEFAULT_NODE_HEIGHT);
                diagram.add(softGoalNode, true);
                toggleGestureView(gestureMenuItem);
            }
            else if ("Belief".equalsIgnoreCase(result)) {
                Toast.makeText(this, "Belief", Toast.LENGTH_LONG).show();
                BeliefNode beliefNode = new BeliefNode(diagram, this);
                beliefNode.setBounds(centerX, centerY,
                        DEFAULT_NODE_WIDTH, DEFAULT_NODE_HEIGHT);
                diagram.add(beliefNode, true);
                toggleGestureView(gestureMenuItem);
            }
            else if ("Resource".equalsIgnoreCase(result)) {
                Toast.makeText(this, "Resource", Toast.LENGTH_LONG).show();
                ResourceNode resourceNode = new ResourceNode(diagram, this);
                resourceNode.setBounds(centerX, centerY,
                        DEFAULT_NODE_WIDTH, DEFAULT_NODE_HEIGHT);
                diagram.add(resourceNode, true);
                toggleGestureView(gestureMenuItem);
            }
            else if ("Task".equalsIgnoreCase(result)) {
                Toast.makeText(this, "Task", Toast.LENGTH_LONG).show();
                TaskNode taskNode = new TaskNode(diagram, this);
                taskNode.setBounds(centerX, centerY,
                        DEFAULT_NODE_WIDTH, DEFAULT_NODE_HEIGHT);
                diagram.add(taskNode, true);
                toggleGestureView(gestureMenuItem);
            }
            else if ("Goal".equalsIgnoreCase(result)) {
                Toast.makeText(this, "Goal", Toast.LENGTH_LONG).show();
                GoalNode goalNode = new GoalNode(diagram, this);
                goalNode.setBounds(centerX, centerY,
                        DEFAULT_NODE_WIDTH, DEFAULT_NODE_HEIGHT);
                diagram.add(goalNode, true);
                toggleGestureView(gestureMenuItem);
            }
        }
    }
}
