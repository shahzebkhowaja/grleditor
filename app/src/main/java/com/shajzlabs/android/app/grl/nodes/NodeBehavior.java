package com.shajzlabs.android.app.grl.nodes;

import com.mindfusion.diagramming.DiagramView;
import com.mindfusion.diagramming.LinkNodesBehavior;


public class NodeBehavior extends LinkNodesBehavior
{
    public NodeBehavior(DiagramView diagramView) {
        super(diagramView);
    }

    @Override
    protected com.mindfusion.diagramming.DiagramNode createNode() {
        return new GoalNode(getDiagram(), getDiagramView().getContext());
    }
}