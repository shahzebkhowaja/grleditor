package com.shajzlabs.android.app.grl.nodes;

import com.mindfusion.diagramming.Brush;
import com.mindfusion.diagramming.DiagramNodeProperties;
import com.mindfusion.diagramming.Pen;

public class NodeProperties extends DiagramNodeProperties
{
    String Text;
    String Value;

    Pen Stroke;
    Brush Fill;
}
