package com.shajzlabs.android.app.grl.nodes;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PointF;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.mindfusion.diagramming.Align;
import com.mindfusion.diagramming.Brush;
import com.mindfusion.diagramming.CompositeNode;
import com.mindfusion.diagramming.Diagram;
import com.mindfusion.diagramming.DiagramItemProperties;
import com.mindfusion.diagramming.HandlesStyle;
import com.mindfusion.diagramming.Pen;
import com.mindfusion.diagramming.SolidBrush;
import com.mindfusion.diagramming.components.ShapeComponent;
import com.mindfusion.diagramming.components.TextComponent;
import com.mindfusion.diagramming.components.XmlLoader;
import com.mindfusion.drawing.Color;
import com.shajzlabs.android.app.grl.R;

import java.io.InputStream;
import java.util.Scanner;

import timber.log.Timber;

public class SoftGoalNode extends CompositeNode {

    public final static String DEFAULT_TEXT = "Soft Goal Node";
    private final static String DEFAULT_VALUE = "100";
    private static final String SOFT_GOAL_NODE_XML = "SoftGoalNode.xml";

    private ShapeComponent shape;
    private TextComponent text;
    private TextComponent value;

    private Context context;

    public SoftGoalNode(Diagram diagram, Context context) {
        super(diagram);
        this.context = context;
        String xml = loadXmlFile(SOFT_GOAL_NODE_XML, context);
        getComponents().add(XmlLoader.load(xml, null, null));
        findComponents();

        setText(DEFAULT_TEXT);
        setValue(DEFAULT_VALUE);

        text.getTextFormat().setHorizontalAlign(Align.Center);
        value.getTextFormat().setHorizontalAlign(Align.Center);
        text.getTextFormat().setVerticalAlign(Align.Center);
        value.getTextFormat().setVerticalAlign(Align.Center);
        shape.setIsOutline(true);
        setFill(new SolidBrush(Color.WHITE));
        setStroke(new Pen(1, Color.BLACK));
        setVisible(true);
        setHandlesStyle(HandlesStyle.EasyMove);
    }

    // empty constructor required for deserialization
    public SoftGoalNode() {
    }

    public SoftGoalNode(SoftGoalNode prototype) {
        super(prototype);
        findComponents();
    }

    @Override
    protected void onLoad(Diagram diagram) {
        super.onLoad(diagram);
        findComponents();
    }

    void findComponents() {
        // find components by name
        shape = (ShapeComponent) findComponent("Shape");
        text = (TextComponent) findComponent("Text");
        value = (TextComponent) findComponent("Value");
    }

    @Override
    protected void saveProperties(DiagramItemProperties props) {
        super.saveProperties(props);

        NodeProperties state = (NodeProperties)props;
        state.Text = getText();
        state.Value = getValue();
        state.Stroke = getStroke();
        state.Fill = getFill();
    }

    @Override
    protected void restoreProperties(DiagramItemProperties props) {
        super.restoreProperties(props);

        NodeProperties state = (NodeProperties)props;
        setText(state.Text);
        setValue(state.Value);
        setStroke(state.Stroke);
        setFill(state.Fill);
    }

    public Pen getStroke() {
        return shape.getPen();
    }

    public void setStroke(Pen pen) {
        shape.setPen(pen);
        this.repaint();
    }

    public Brush getFill() {
        return shape.getBrush();
    }

    public void setFill(Brush brush) {
        shape.setBrush(brush);
    }

    public String getText() {
        return text.getText();
    }

    public void setText(String text) {
        this.text.setText(text);
    }

    public String getValue() {
        String val = value.getText();

        if(val.startsWith("(")) {
            val = val.replace("(", "");

        }

        if(val.endsWith(")")) {
            val = val.replace(")", "");
        }

        return val;
    }

    public void setValue(String value) {
        this.value.setText(String.format("(%s)", value));
    }

    public void setXml(String xmlname){
        String xml = loadXmlFile(xmlname, context);
        getComponents().add(XmlLoader.load(xml, null, null));
    }

    private String loadXmlFile(String filepath, Context context) {
        InputStream istream = null;
        String xml = null;
        try
        {
            istream = context.getAssets().open(filepath);
            Scanner myScanner = new Scanner(istream);
            myScanner.useDelimiter("\\A");
            xml = myScanner.next();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return xml;
    }

    @Override
    protected void onDoubleClick(int mouseButton, PointF pointerPosition) {
        editNodeTitle();
    }

    public void editNodeTitle() {
        if (context != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Edit Title");

            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_edit_node, null);

            final EditText input = (EditText) view.findViewById(R.id.title);
            builder.setView(view);

            // get text from node
            input.setText(getText());

            // Set up the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    setText(input.getText().toString());
                    invalidate();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();
        }
        else {
            Timber.d("context: NULL");
        }
    }

}
