package com.shajzlabs.android.app.grl.viewcontrol;

import android.content.Context;
import android.gesture.GestureOverlayView;
import android.util.AttributeSet;

import com.mindfusion.diagramming.DiagramView;

public class GestureDrawView extends DiagramView {

    private GestureOverlayView gestureOverlayView;

    public GestureDrawView(Context context) {
        super(context);
    }

    public GestureDrawView(Context context, AttributeSet attributes) {
        super(context, attributes);
    }


}
